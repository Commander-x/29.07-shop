import React, { Component } from 'react';
import {connect}    from 'react-redux';
import { Link } from 'react-router-dom';

class Product extends Component {

    add = (el)=> {
        let pkey = el.target.dataset.key;
        let Product = this.props.products[pkey];
        this.props.addCart(Product);
    }

  render() {

      return (
        <div className="col-sm-3">
            <div className="product text-center">
                <h3>{this.props.data.name}</h3>
                <div className="image">
                    <Link to={"/product/" + this.props.data.slug} >
                        <img className="img-fluid" src={'/img/'+this.props.data.image} />
                    </Link>    
                </div>
                <div className="price">
                    {this.props.data.price}
                </div>
                <div className="manufacturer">
                    {this.props.data.manufacturer}
                </div>
                <div className="description">
                    {this.props.data.description}
                </div>
                {
                  this.props.data.quantity 
                  ? <button onClick={this.add} data-key={this.props.pkey} type="button" className="btn btn-primary">Add to cart</button>
                  : <button type="button" className="btn btn-success">Pre order</button>
                }
            </div>
        </div>
      );
  }


}


export default connect(
    state => ({
       products:state.app.products
    }),
    dispatch => ({
       addCart:(Product)=>{
            dispatch({type:'ADD_CART', data:Product});
       }
    })
)(Product);
