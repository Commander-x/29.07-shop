import React, { Component } from 'react';
import Product     from './Product';
import {getProductsAction} from '../actions/app';
import {connect}   from  'react-redux';

class Products extends Component {

    constructor(params)
    {
        super(params);

        this.state = 
        {
            str:''
        }
    }

    componentDidMount()
    {
        this.props.getProducts();
    }

    showProducts()
    {
        let filtered = [];

        for(let j in this.props.products)
        {
            let product = this.props.products[j];

            if(this.state.str)
            {
                if(JSON.stringify(product).toLowerCase().search(this.state.str.toLowerCase()) !== -1)
                {
                    filtered.push(<Product key={j} pkey={j} data={product} addCart = {this.props.addCart} />);
                }
                else{
                    continue;
                }
            }
            else{
                filtered.push(<Product key={j} pkey={j} data={product} addCart = {this.props.addCart} />);
            }
        }

       return filtered;
    }

    filterProducts = (el) => {
        this.setState({str:el.target.value});
    }
    
    render() {
        
        return (
            <div className="products row page">
                <h3 className="col-md-12" >Products</h3>
                <div className="col-md-12 my-4">
                    <input onInput={this.filterProducts} className="form-control" placeholder="Search products" />
                </div>
                {this.showProducts()}
            </div>
        );
    }
}

export default connect(
    state => ({
        products: state.app.products
    }),
    dispatch => ({
        getProducts:() => {            
            dispatch(getProductsAction());
        }
    })
)(Products);
